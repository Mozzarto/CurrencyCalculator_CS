﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    public partial class mainWindow : Form
    {
        Validator valid_number = new Valid_number();
        Validator valid_string = new Valid_string();
        private int menunumber = 1;
        private bool mouseDown;
        private Point lastLocation;

        public mainWindow()
        {
            InitializeComponent();
            CleanMenu();
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new System.Drawing.Rectangle(0, 0, width, height);
            var destImage = new System.Drawing.Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = System.Drawing.Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
                {
                    wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public string TextBoxCur1
        {
            get { return this.textBoxCur1.Text; }
        }

        public string TextBoxCur2
        {
            get { return this.textBoxCur2.Text; }
        }

        public string TextBoxAmount
        {
            get { return this.textBoxAmount.Text; }
        }

        public string TextBoxResult
        {
            get { return this.textBoxResult.Text; }
            set { textBoxResult.Text = value; }
        }

        public string ErrorText
        {
            get { return this.errorText.Text; }
            set { errorText.Text = value; }
        }

        public string LabelInfo
        {
            get { return this.labelCurr.Text; }
            set { labelCurr.Text = value; }
        }

        private void labelExit_MouseEnter(object sender, EventArgs e)
        {
            this.labelExit.Image = ResizeImage(global::CurrencyCalculator.Properties.Resources.Xmouse, 22, 22);
        }

        private void labelExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labelExit_MouseLeave(object sender, EventArgs e)
        {
            this.labelExit.Image = ResizeImage(global::CurrencyCalculator.Properties.Resources.X, 22, 22);
        }

        private void buttonMenu1_Click(object sender, EventArgs e)
        {
            menunumber = 1;
            CleanMenu();
        }
        private void buttonMenu1_MouseEnter(object sender, EventArgs e)
        {
            this.buttonMenu1.Image = global::CurrencyCalculator.Properties.Resources.kantormouse;
        }
        private void buttonMenu1_MouseLeave(object sender, EventArgs e)
        {
            CleanMenu();
        }

        private void buttonMenu2_Click(object sender, EventArgs e)
        {
            menunumber = 2;
            CleanMenu();
        }
        private void buttonMenu2_MouseEnter(object sender, EventArgs e)
        {
            this.buttonMenu2.Image = global::CurrencyCalculator.Properties.Resources.walutymouse;

        }
        private void buttonMenu2_MouseLeave(object sender, EventArgs e)
        {
            CleanMenu();
        }

        private void labelMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void labelMini_MouseEnter(object sender, EventArgs e)
        {
            this.labelMini.Image = ResizeImage(global::CurrencyCalculator.Properties.Resources.minimouse, 22, 22);
        }

        private void labelMini_MouseLeave(object sender, EventArgs e)
        {
            this.labelMini.Image = ResizeImage(global::CurrencyCalculator.Properties.Resources.mini, 22, 22);
        }

        private void buttonCalc_MouseEnter_1(object sender, EventArgs e)
        {
            this.buttonCalc.Image = global::CurrencyCalculator.Properties.Resources.buttonmouse;
        }

        private void buttonCalc_MouseLeave_1(object sender, EventArgs e)
        {
            this.buttonCalc.Image = global::CurrencyCalculator.Properties.Resources.buttonnoncl;

        }

        private void buttonCalc_Click_1(object sender, EventArgs e)
        {
            Calc();
        }

        

        private void upPanel_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void upPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void upPanel_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void textBoxCur1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Calc();
                e.SuppressKeyPress = true;
            }
        }
        private void textBoxCur2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Calc();
                e.SuppressKeyPress = true;
            }
        }

        private void textBoxAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Calc();
                e.SuppressKeyPress = true;
            }
        }

        public void CleanMenu()
        {
            if (menunumber == 1)
            {
                this.mainPanel.Visible = true;
                this.buttonMenu1.Image = global::CurrencyCalculator.Properties.Resources.kantorcl;
            }
            else
            {
                this.mainPanel.Visible = false;
                this.buttonMenu1.Image = global::CurrencyCalculator.Properties.Resources.kantornoncl;
            }
            if (menunumber == 2)
            {
                this.panelList.Visible = true;
                this.buttonMenu2.Image = global::CurrencyCalculator.Properties.Resources.walutycl;
            }
            else
            {
                this.panelList.Visible = false;
                this.buttonMenu2.Image = global::CurrencyCalculator.Properties.Resources.walutynoncl;
            }


        }
        private void Calc()
        {
            if (TextBoxAmount.Equals("") || TextBoxCur1.Equals("") || TextBoxCur2.Equals(""))
            {
                this.ErrorText = "PUSTE POLA";
                this.errorText.Visible = true;
            }
            else if (valid_number.Check_input(TextBoxAmount) == false ||
                        valid_string.Check_input(TextBoxCur1) == false ||
                        valid_string.Check_input(TextBoxCur2) == false)
            {
                this.ErrorText = "NIEPOPRAWNE DANE";
                this.errorText.Visible = true;
            }
            else
            {
                this.errorText.Visible = false;
                this.TextBoxResult = Calculate.Calc().ToString("F");
            }
        }
    }
}
