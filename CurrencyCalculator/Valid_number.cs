﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class Valid_number : Validator
    {
        public bool Check_input(string input)
        {
            Boolean boolean = false;
            try
            {
                Double.Parse(input.Replace(",", "."), CultureInfo.InvariantCulture);
                boolean = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                boolean = false;
            }

            if (input.Equals(""))
            {
                boolean = false;
            }

            if (Double.Parse(input.Replace(",", "."), CultureInfo.InvariantCulture) < 0)
            {
                boolean = false;
            }
            return boolean;
        }
    }
}
