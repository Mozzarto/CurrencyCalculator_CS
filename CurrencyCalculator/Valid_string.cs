﻿using System;
using System.Linq;

namespace CurrencyCalculator
{
    class Valid_string : Validator
    {
        public bool Check_input(string input)
        {
            Boolean boolean = false;
            char[] chaObject = input.ToCharArray();
            for (int j = 0; j < Curr_list.currencies.Count; j++)
            {
                char[] chaCode = Curr_list.currencies[j].Code.ToCharArray();
                char[] chaName = Curr_list.currencies[j].Name.ToCharArray();
                if (chaObject.Length == chaCode.Length || chaObject.Length == chaName.Length)
                {
                    if (chaCode.SequenceEqual(chaObject) || chaName.SequenceEqual(chaObject))
                    {
                        boolean = true;
                    }

                }
            }
            if (input.Equals(""))
            {
                boolean = false;
            }
            return boolean;
        }
    }
}
