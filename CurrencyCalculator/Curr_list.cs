﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class Curr_list
    {
        public static List<Currency> currencies = new List<Currency>();
        public static String currencyText = "";

        public static void AddPLN()
        {
            currencies.Insert(0, new Currency("Polski Złoty", 1, "PLN", 1));
        }

        public static void CreateTextFromList()
        {
            for (int i = 0; i < currencies.Count; i++)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("Nazwa waluty: ");
                stringBuilder.Append(currencies[i].Name);
                stringBuilder.Append("\n");
                stringBuilder.Append("Kod waluty: ");
                stringBuilder.Append(currencies[i].Code);
                if (i < currencies.Count - 1)
                    stringBuilder.Append("\n----------\n");
                currencyText += stringBuilder;
            }
        }

        public static Boolean GetCostByCode()
        {
            Boolean boolean = false;
            for (int j = 0; j < Curr_list.currencies.Count; j++)
            {
                char[] chaCode = Curr_list.currencies[j].Code.ToCharArray();
                if (chaCode.SequenceEqual(Program.mainTemp.TextBoxCur1.ToCharArray()))
                {
                    Calculate.first_cur = new Currency(
                        Curr_list.currencies[j].Name, 
                        Curr_list.currencies[j].Multiplier,
                        Curr_list.currencies[j].Code,
                        Curr_list.currencies[j].Rate);
                    boolean = true;
                }
                if (chaCode.SequenceEqual(Program.mainTemp.TextBoxCur2.ToCharArray()))
                {
                    Calculate.sec_cur = new Currency(
                        Curr_list.currencies[j].Name,
                        Curr_list.currencies[j].Multiplier,
                        Curr_list.currencies[j].Code,
                        Curr_list.currencies[j].Rate);
                    boolean = true;
                }
            }
            return boolean;
        }

        public static Boolean GetCostByName()
        {
            Boolean boolean = false;
            for (int j = 0; j < Curr_list.currencies.Count; j++)
            {
                char[] chaName = Curr_list.currencies[j].Name.ToCharArray();
                if (chaName.SequenceEqual(Program.mainTemp.TextBoxCur1.ToCharArray()))
                {
                    Calculate.first_cur = new Currency(
                        Curr_list.currencies[j].Name,
                        Curr_list.currencies[j].Multiplier,
                        Curr_list.currencies[j].Code,
                        Curr_list.currencies[j].Rate);
                    boolean = true;
                }
                if (chaName.SequenceEqual(Program.mainTemp.TextBoxCur2.ToCharArray()))
                {
                    Calculate.sec_cur = new Currency(
                        Curr_list.currencies[j].Name,
                        Curr_list.currencies[j].Multiplier,
                        Curr_list.currencies[j].Code,
                        Curr_list.currencies[j].Rate);
                    boolean = true;
                }
            }
            return boolean;
        }
    }
}
