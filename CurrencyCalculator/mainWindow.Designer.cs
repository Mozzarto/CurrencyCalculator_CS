﻿using System.Drawing;

namespace CurrencyCalculator
{
    partial class mainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainWindow));
            this.menuPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonMenu1 = new System.Windows.Forms.Label();
            this.buttonMenu2 = new System.Windows.Forms.Label();
            this.upPanel = new System.Windows.Forms.Panel();
            this.labelAppName = new System.Windows.Forms.Label();
            this.labelIco = new System.Windows.Forms.Label();
            this.labelMini = new System.Windows.Forms.Label();
            this.labelExit = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.panelList = new System.Windows.Forms.Panel();
            this.labelCurr = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.arrow = new System.Windows.Forms.Label();
            this.errorText = new System.Windows.Forms.Label();
            this.buttonCalc = new System.Windows.Forms.Label();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.textBoxCur2 = new System.Windows.Forms.TextBox();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.textBoxCur1 = new System.Windows.Forms.TextBox();
            this.menuPanel.SuspendLayout();
            this.upPanel.SuspendLayout();
            this.panelList.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.menuPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuPanel.Controls.Add(this.buttonMenu1);
            this.menuPanel.Controls.Add(this.buttonMenu2);
            this.menuPanel.Location = new System.Drawing.Point(0, 30);
            this.menuPanel.Margin = new System.Windows.Forms.Padding(0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(460, 30);
            this.menuPanel.TabIndex = 0;
            // 
            // buttonMenu1
            // 
            this.buttonMenu1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonMenu1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMenu1.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonMenu1.Image = global::CurrencyCalculator.Properties.Resources.kantornoncl;
            this.buttonMenu1.Location = new System.Drawing.Point(0, 0);
            this.buttonMenu1.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMenu1.Name = "buttonMenu1";
            this.buttonMenu1.Size = new System.Drawing.Size(60, 30);
            this.buttonMenu1.TabIndex = 0;
            this.buttonMenu1.Click += new System.EventHandler(this.buttonMenu1_Click);
            this.buttonMenu1.MouseEnter += new System.EventHandler(this.buttonMenu1_MouseEnter);
            this.buttonMenu1.MouseLeave += new System.EventHandler(this.buttonMenu1_MouseLeave);
            // 
            // buttonMenu2
            // 
            this.buttonMenu2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonMenu2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMenu2.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonMenu2.Image = global::CurrencyCalculator.Properties.Resources.walutynoncl;
            this.buttonMenu2.Location = new System.Drawing.Point(60, 0);
            this.buttonMenu2.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMenu2.Name = "buttonMenu2";
            this.buttonMenu2.Size = new System.Drawing.Size(60, 30);
            this.buttonMenu2.TabIndex = 0;
            this.buttonMenu2.Click += new System.EventHandler(this.buttonMenu2_Click);
            this.buttonMenu2.MouseEnter += new System.EventHandler(this.buttonMenu2_MouseEnter);
            this.buttonMenu2.MouseLeave += new System.EventHandler(this.buttonMenu2_MouseLeave);
            // 
            // upPanel
            // 
            this.upPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.upPanel.Controls.Add(this.labelAppName);
            this.upPanel.Controls.Add(this.labelIco);
            this.upPanel.Controls.Add(this.labelMini);
            this.upPanel.Controls.Add(this.labelExit);
            this.upPanel.Location = new System.Drawing.Point(0, 0);
            this.upPanel.Name = "upPanel";
            this.upPanel.Size = new System.Drawing.Size(460, 30);
            this.upPanel.TabIndex = 2;
            this.upPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.upPanel_MouseDown);
            this.upPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.upPanel_MouseMove);
            this.upPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.upPanel_MouseUp);
            // 
            // labelAppName
            // 
            this.labelAppName.Font = new System.Drawing.Font("Comfortaa", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAppName.ForeColor = System.Drawing.Color.Silver;
            this.labelAppName.Location = new System.Drawing.Point(30, 0);
            this.labelAppName.Name = "labelAppName";
            this.labelAppName.Size = new System.Drawing.Size(150, 30);
            this.labelAppName.TabIndex = 3;
            this.labelAppName.Text = "Kantor walutowy";
            // 
            // labelIco
            // 
            this.labelIco.Image = ((System.Drawing.Image)(resources.GetObject("labelIco.Image")));
            this.labelIco.Location = new System.Drawing.Point(3, 0);
            this.labelIco.Margin = new System.Windows.Forms.Padding(0);
            this.labelIco.Name = "labelIco";
            this.labelIco.Size = new System.Drawing.Size(30, 30);
            this.labelIco.TabIndex = 2;
            this.labelIco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMini
            // 
            this.labelMini.Image = ((System.Drawing.Image)(resources.GetObject("labelMini.Image")));
            this.labelMini.Location = new System.Drawing.Point(405, -1);
            this.labelMini.Name = "labelMini";
            this.labelMini.Size = new System.Drawing.Size(30, 30);
            this.labelMini.TabIndex = 1;
            this.labelMini.Click += new System.EventHandler(this.labelMini_Click);
            this.labelMini.MouseEnter += new System.EventHandler(this.labelMini_MouseEnter);
            this.labelMini.MouseLeave += new System.EventHandler(this.labelMini_MouseLeave);
            // 
            // labelExit
            // 
            this.labelExit.Image = ((System.Drawing.Image)(resources.GetObject("labelExit.Image")));
            this.labelExit.Location = new System.Drawing.Point(430, 0);
            this.labelExit.Name = "labelExit";
            this.labelExit.Size = new System.Drawing.Size(30, 30);
            this.labelExit.TabIndex = 0;
            this.labelExit.Click += new System.EventHandler(this.labelExit_Click);
            this.labelExit.MouseEnter += new System.EventHandler(this.labelExit_MouseEnter);
            this.labelExit.MouseLeave += new System.EventHandler(this.labelExit_MouseLeave);
            // 
            // panelList
            // 
            this.panelList.AutoScroll = true;
            this.panelList.Controls.Add(this.labelCurr);
            this.panelList.Location = new System.Drawing.Point(0, 60);
            this.panelList.Margin = new System.Windows.Forms.Padding(0);
            this.panelList.Name = "panelList";
            this.panelList.Size = new System.Drawing.Size(458, 438);
            this.panelList.TabIndex = 6;
            // 
            // labelCurr
            // 
            this.labelCurr.AutoSize = true;
            this.labelCurr.Font = new System.Drawing.Font("Comfortaa", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCurr.ForeColor = System.Drawing.Color.White;
            this.labelCurr.Location = new System.Drawing.Point(0, 0);
            this.labelCurr.Name = "labelCurr";
            this.labelCurr.Size = new System.Drawing.Size(0, 26);
            this.labelCurr.TabIndex = 0;
            // 
            // mainPanel
            // 
            this.mainPanel.BackgroundImage = global::CurrencyCalculator.Properties.Resources.kantor;
            this.mainPanel.Controls.Add(this.arrow);
            this.mainPanel.Controls.Add(this.errorText);
            this.mainPanel.Controls.Add(this.buttonCalc);
            this.mainPanel.Controls.Add(this.textBoxResult);
            this.mainPanel.Controls.Add(this.textBoxCur2);
            this.mainPanel.Controls.Add(this.textBoxAmount);
            this.mainPanel.Controls.Add(this.textBoxCur1);
            this.mainPanel.Location = new System.Drawing.Point(0, 60);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(460, 440);
            this.mainPanel.TabIndex = 0;
            // 
            // arrow
            // 
            this.arrow.Enabled = false;
            this.arrow.Image = global::CurrencyCalculator.Properties.Resources.arrow;
            this.arrow.Location = new System.Drawing.Point(194, 176);
            this.arrow.Name = "arrow";
            this.arrow.Size = new System.Drawing.Size(70, 23);
            this.arrow.TabIndex = 7;
            // 
            // errorText
            // 
            this.errorText.Font = new System.Drawing.Font("Open Sans", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.errorText.ForeColor = System.Drawing.Color.Red;
            this.errorText.Location = new System.Drawing.Point(0, 406);
            this.errorText.Name = "errorText";
            this.errorText.Size = new System.Drawing.Size(460, 23);
            this.errorText.TabIndex = 5;
            this.errorText.Text = "NIEPOPRAWNE DANE";
            if(XML_Provider.errorBase == 1 && SQL_Provider.errorBase == 1)
            {
                errorText.Visible = true;
                errorText.Text = "BRAK POŁĄCZENIA Z SERWEREM";
                buttonCalc.Cursor = System.Windows.Forms.Cursors.Default;
                buttonCalc.Enabled = false;
            }
            else
            {
                errorText.Visible = false;
            }
            this.errorText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonCalc
            // 
            this.buttonCalc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCalc.Image = global::CurrencyCalculator.Properties.Resources.buttonnoncl;
            this.buttonCalc.Location = new System.Drawing.Point(156, 345);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(150, 40);
            this.buttonCalc.TabIndex = 4;
            this.buttonCalc.Click += new System.EventHandler(this.buttonCalc_Click_1);
            this.buttonCalc.MouseEnter += new System.EventHandler(this.buttonCalc_MouseEnter_1);
            this.buttonCalc.MouseLeave += new System.EventHandler(this.buttonCalc_MouseLeave_1);
            // 
            // textBoxResult
            // 
            this.textBoxResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))));
            this.textBoxResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResult.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxResult.Enabled = false;
            this.textBoxResult.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxResult.ForeColor = System.Drawing.Color.Gray;
            this.textBoxResult.Location = new System.Drawing.Point(272, 278);
            this.textBoxResult.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(140, 26);
            this.textBoxResult.TabIndex = 3;
            this.textBoxResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxCur2
            // 
            this.textBoxCur2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))));
            this.textBoxCur2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxCur2.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCur2.ForeColor = System.Drawing.Color.Silver;
            this.textBoxCur2.Location = new System.Drawing.Point(272, 207);
            this.textBoxCur2.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxCur2.Name = "textBoxCur2";
            this.textBoxCur2.Size = new System.Drawing.Size(140, 26);
            this.textBoxCur2.TabIndex = 1;
            this.textBoxCur2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxCur2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxCur2_KeyDown);
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))));
            this.textBoxAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAmount.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxAmount.ForeColor = System.Drawing.Color.Silver;
            this.textBoxAmount.Location = new System.Drawing.Point(49, 278);
            this.textBoxAmount.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(140, 26);
            this.textBoxAmount.TabIndex = 2;
            this.textBoxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAmount_KeyDown);
            // 
            // textBoxCur1
            // 
            this.textBoxCur1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))));
            this.textBoxCur1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxCur1.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCur1.ForeColor = System.Drawing.Color.Silver;
            this.textBoxCur1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxCur1.Location = new System.Drawing.Point(49, 207);
            this.textBoxCur1.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxCur1.Name = "textBoxCur1";
            this.textBoxCur1.Size = new System.Drawing.Size(140, 26);
            this.textBoxCur1.TabIndex = 0;
            this.textBoxCur1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxCur1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxCur1_KeyDown);
            // 
            // mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(78)))), ((int)(((byte)(78)))));
            this.ClientSize = new System.Drawing.Size(460, 500);
            this.Controls.Add(this.upPanel);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.panelList);
            this.Controls.Add(this.menuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = global::CurrencyCalculator.Properties.Resources.icoapp1;
            this.Name = "mainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kantor walutowy";
            this.menuPanel.ResumeLayout(false);
            this.upPanel.ResumeLayout(false);
            this.panelList.ResumeLayout(false);
            this.panelList.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel menuPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel upPanel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Label buttonMenu1;
        private System.Windows.Forms.Label buttonMenu2;
        private System.Windows.Forms.Label labelExit;
        private System.Windows.Forms.Label labelMini;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.TextBox textBoxCur2;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.TextBox textBoxCur1;
        private System.Windows.Forms.Label labelIco;
        private System.Windows.Forms.Label labelAppName;
        private System.Windows.Forms.Label buttonCalc;
        private System.Windows.Forms.Label errorText;
        private System.Windows.Forms.Panel panelList;
        private System.Windows.Forms.Label labelCurr;
        private System.Windows.Forms.Label arrow;
    }


}

