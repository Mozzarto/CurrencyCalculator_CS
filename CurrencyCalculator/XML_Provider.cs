﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CurrencyCalculator
{
    class XML_Provider : IProvider
    {
        public static String noconnection = "";
        public static int errorBase = 0;

        public void GetData()
        {
            try
            {
                const string URL = "http://www.nbp.pl/kursy/xml/LastA.xml";

                XmlReader reader = XmlReader.Create(URL);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name.Equals("tabela_kursow"))
                        break;
                    if (reader.NodeType == XmlNodeType.Element)
                        switch (reader.Name)
                        {
                            case "pozycja":
                                XDocument xDoc = XDocument.Parse("<pozycja>" + reader.ReadInnerXml() + "</pozycja>");
                                Curr_list.currencies.Add(new Currency(
                                    xDoc.Root.Elements("nazwa_waluty").FirstOrDefault().Value.ToString(),
                                    Int32.Parse(xDoc.Root.Elements("przelicznik").FirstOrDefault().Value),
                                    xDoc.Root.Elements("kod_waluty").FirstOrDefault().Value.ToString(),
                                    Double.Parse(xDoc.Root.Elements("kurs_sredni").FirstOrDefault().Value)
                                    ));
                                break;
                        }
                }
                Curr_list.CreateTextFromList();
            }
            catch(Exception e)
            {
                noconnection = "XML_Provider.java: no connection to server";
                Console.WriteLine(noconnection);
                Console.WriteLine(e);
                errorBase = 1;
            }
        }
    }
}

