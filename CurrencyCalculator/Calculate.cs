﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class Calculate
    {
        public static Currency first_cur;
        public static Currency sec_cur;

        public static double Calc()
        {
            if (Curr_list.GetCostByCode() == true)
                Curr_list.GetCostByCode();
            else if (Curr_list.GetCostByName() == true)
                Curr_list.GetCostByName();

            double result = 0;

            if (first_cur.Code.Equals("PLN") || first_cur.Name.Equals("Polski Złoty"))
            {
                result = Double.Parse(Program.mainTemp.TextBoxAmount.Replace(",", "."), CultureInfo.InvariantCulture) / (sec_cur.Multiplier * sec_cur.Rate);
            }
            else if (sec_cur.Code.Equals("PLN") || sec_cur.Name.Equals("Polski Złoty"))
            {
                result = (Double.Parse(Program.mainTemp.TextBoxAmount.Replace(",", "."), CultureInfo.InvariantCulture) * first_cur.Rate) / first_cur.Multiplier;
            }
            else
            {
                double temp = (Double.Parse(Program.mainTemp.TextBoxAmount.Replace(",", "."), CultureInfo.InvariantCulture) * first_cur.Rate) / first_cur.Multiplier;
                result = temp / (sec_cur.Multiplier * sec_cur.Rate);
            }
            return result;
        }
    }
}
