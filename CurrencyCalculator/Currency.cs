﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class Currency
    {
        public Currency(string name, int multiplier, string code, double rate)
        {
            this.Name = name;
            this.Multiplier = multiplier;
            this.Code = code;
            this.Rate = rate;
        }

        public string Code { get; }
        public string Name { get; }
        public int Multiplier { get; set; }
        public double Rate { get; set; }
    }
}
