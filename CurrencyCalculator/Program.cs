﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrencyCalculator
{
    static class Program
    {
        public static mainWindow mainTemp;

        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Curr_list.AddPLN();
            IProvider provider = new XML_Provider();
            provider.GetData();
            if(XML_Provider.errorBase == 1)
            {
                IProvider provider2 = new SQL_Provider();
                provider2.GetData();
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mainWindow main = new mainWindow();
            main.LabelInfo = Curr_list.currencyText;
            mainTemp = main;
            Application.Run(main);
        }
    }
}
