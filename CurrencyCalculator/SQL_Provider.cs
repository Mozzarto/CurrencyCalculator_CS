﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyCalculator
{
    class SQL_Provider : IProvider
    {
        public static String noconnection = "";
        public static int errorBase = 0;

        public void GetData()
        {
            try
            {
                SqlConnection connection = new SqlConnection(@"Data source=;
                                                             database=;
                                                             User id=;
                                                             Password=;");
                connection.Open();
                SqlCommand sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = "SELECT * FROM waluty";
                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    Curr_list.currencies.Add(new Currency(
                        reader["nazwa_waluty"].ToString(), 
                        Int32.Parse(reader["przelicznik"].ToString()),
                        reader["kod_waluty"].ToString(),
                        Double.Parse(reader["kurs_sredni"].ToString())
                        ));
                }
                reader.Close();
                connection.Close();
            }

            catch (SqlException e)
            {
                noconnection = "SQL_Provider.java: no connection to server";
                Console.WriteLine(noconnection);
                Console.WriteLine(e);
                errorBase = 1;
            }
        }
    }
}
